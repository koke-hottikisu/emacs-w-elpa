(need-package '(
		company
		company-box
		))

(global-company-mode) ; 全バッファで有効にする

;; hooks
(add-hook 'company-mode-hook 'company-box-mode)

;; bind
; (global-set-key (kbd "C-M-i") 'company-complete)
; (global-set-key (kbd "<tab>") 'company-indent-or-complete-common)
(define-key company-active-map (kbd "C-n") 'company-select-next) ;; C-n, C-pで補完候補を次/前の候補を選択
(define-key company-active-map (kbd "C-p") 'company-select-previous)
(define-key company-search-map (kbd "C-n") 'company-select-next)
(define-key company-search-map (kbd "C-p") 'company-select-previous)
(define-key company-active-map (kbd "C-s") 'company-filter-candidates) ;; C-sで絞り込む
(define-key company-active-map (kbd "C-f") 'company-complete-selection) ;; C-fで候補を設定
(define-key emacs-lisp-mode-map (kbd "C-M-i") 'company-complete) ;; 各種メジャーモードでも C-M-iで company-modeの補完を使う
(global-set-key (kbd "M-*") 'xref-pop-marker-stack)
(global-set-key (kbd "M-.") 'xref-find-definitions)
(global-set-key (kbd "M-/") 'xref-find-references)

;; common
(setq company-idle-delay 0)    ; 遅延なし
(setq company-minimum-prefix-length 1) ; デフォルトは4
(setq company-selection-wrap-around t)    ; 候補の最後の次は先頭に戻る
(setq completion-ignore-case t)
(setq company-dabbrev-downcase nil)
(setq company-dabbrev-char-regexp "\\(\\sw\\|\\s_\\|_\\|-\\)")    ; -や_などを含む語句も補完
(setq lsp-prefer-capf t)

;; 検索結果(default)
(setq company-backends '(company-bbdb company-eclim company-semantic company-clang company-xcode company-cmake company-capf company-files
			  (company-dabbrev-code company-gtags company-etags company-keywords)
			  company-oddmuse company-dabbrev))

;; color
(set-face-attribute 'company-tooltip nil
		    :foreground "black" :background "gray60")
(set-face-attribute 'company-tooltip-common nil
		    :foreground "black" :background "gray60")
(set-face-attribute 'company-tooltip-common-selection nil
		    :foreground "white" :background "steelblue")
(set-face-attribute 'company-tooltip-selection nil
		    :foreground "black" :background "steelblue")
(set-face-attribute 'company-preview-common nil
		    :background nil :foreground "lightgrey" :underline t)
(set-face-attribute 'company-scrollbar-fg nil
		    :background "grey60")
(set-face-attribute 'company-scrollbar-bg nil
		    :background "gray40")
