(require 'package)
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
	("melpa-stable" . "https://stable.melpa.org/packages/")
	("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

; (package-refresh-contents)

(defun need-package (favorite-packages)
  "auto install package if not installed"
  (dolist (package favorite-packages)
    (unless (package-installed-p package)
      (package-install package)))
  )
