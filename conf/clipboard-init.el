(need-package '(
		xclip
		))

(setq x-select-enable-clipboard t)
(xclip-mode 1)
