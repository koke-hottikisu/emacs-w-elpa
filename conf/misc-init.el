(need-package '(
		dockerfile-mode docker-compose-mode))

;; 環境を日本語、UTF-8にする
(set-locale-environment nil)
(set-language-environment "Japanese")
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)

;; キーバインド
(define-key key-translation-map (kbd "C-h") (kbd "<DEL>"))
(define-key key-translation-map (kbd "C-j") (kbd "C-m"))
(global-set-key (kbd "C-x p") 'previous-multiframe-window)

;; テーマ
(load-theme 'misterioso t)

;; font
(setq-default line-spacing 0)
(set-fontset-font (frame-parameter nil 'font)
                  'japanese-jisx0208
		  '("Ricty Diminished" . "unicode-bmp")
                  )
(set-fontset-font (frame-parameter nil 'font)
                  'katakana-jisx0201
		  '("Ricty Diminished" . "unicode-bmp")
		  )

;; 対応する括弧を光らせる
(show-paren-mode 1)

;; "yes or no" の選択を "y or n" にする
(fset 'yes-or-no-p 'y-or-n-p)
