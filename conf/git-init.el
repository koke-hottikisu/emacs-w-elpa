(need-package '(
		magit
		gitignore-mode))

(global-set-key (kbd "C-x m") 'magit-status)
