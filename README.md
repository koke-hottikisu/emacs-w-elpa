# emacs

## install

### usnig apt
```
sudo add-apt-repository ppa:kelleyk/emacs
sudo apt update
sudo apt install emacs26
sudo update-alternative --config emacs
```

### from src
```
wget http://ftp.jaist.ac.jp/pub/GNU/emacs/emacs-27.1.tar.gz
tar -xf emacs-27.1.tar.gz

sudo apt update
sudo apt install libgtk-3-dev
sudo apt install libgif-dev
sudo apt install ncurses-devel
sudo apt install libxml2-dev

cd emacs-27.1
./configure
make -j8
sudo make install
cd ..
```
