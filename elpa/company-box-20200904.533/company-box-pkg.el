(define-package "company-box" "20200904.533" "Company front-end with icons"
  '((emacs "26.0.91")
    (dash "2.13")
    (dash-functional "1.2.0")
    (company "0.9.6")
    (frame-local "0.0.1"))
  :commit "4265eaf9e4d29f8b93e125fa807e2085e8e0ea3b" :keywords
  '("company" "completion" "front-end" "convenience")
  :authors
  '(("Sebastien Chapuis" . "sebastien@chapu.is"))
  :maintainer
  '("Sebastien Chapuis" . "sebastien@chapu.is")
  :url "https://github.com/sebastiencs/company-box")
;; Local Variables:
;; no-byte-compile: t
;; End:
