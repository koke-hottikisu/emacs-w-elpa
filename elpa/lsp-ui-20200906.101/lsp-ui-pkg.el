(define-package "lsp-ui" "20200906.101" "UI modules for lsp-mode"
  '((emacs "26.1")
    (dash "2.14")
    (dash-functional "1.2.0")
    (lsp-mode "6.0")
    (markdown-mode "2.3"))
  :commit "7dbedcc02bfebc88318c939d59530030900e0fc7" :keywords
  '("languages" "tools")
  :authors
  '(("Sebastien Chapuis <sebastien@chapu.is>, Fangrui Song" . "i@maskray.me"))
  :maintainer
  '("Sebastien Chapuis <sebastien@chapu.is>, Fangrui Song" . "i@maskray.me")
  :url "https://github.com/emacs-lsp/lsp-ui")
;; Local Variables:
;; no-byte-compile: t
;; End:
